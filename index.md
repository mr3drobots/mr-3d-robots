---
title: "Accueil"
order: 0
in_menu: true
---
![Logo du site](/images/vert_claire_logo.png)



Mr 3d robots est **un site créé par un passionné de robotique, d'impression 3D et de découpe laser** qui a pour objet de promouvoir la fabrication de robots. 


Les robots sont fabriqué avec les logiciels libre suivant : 
                                                                                                                                                             
<article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/FreeCAD.png">
    </div>
    <div>
      <h2>FreeCAD</h2>
      <p>FreeCAD est un modeleur 3D paramétrique fait pour concevoir des objets réels de toutes les tailles.</p>
      <div>
        <a href="https://framalibre.org/notices/freecad.html">Vers la notice Framalibre</a>
        <a href="http://www.freecadweb.org">Vers le site</a>
      </div>
    </div>
  </article>
                                                                                                                               
  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Inkscape.png" height="50" width="50">
    </div>
    <div>
      <h2>Inkscape</h2>
      <p>Un puissant logiciel de dessin vectoriel.</p>
      <div>
        <a href="https://framalibre.org/notices/inkscape.html">Vers la notice Framalibre</a>
        <a href="https://inkscape.org/fr/">Vers le site</a>
      </div>
    </div>
  </article>
                                                                                                                      
  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Fritzing.jpg">
    </div>
    <div>
      <h2>Fritzing</h2>
      <p>Fritzing est un logiciel de CAO pour l’électronique.</p>
      <div>
        <a href="https://framalibre.org/notices/fritzing.html">Vers la notice Framalibre</a>
        <a href="http://fritzing.org/">Vers le site</a>
      </div>
    </div>
  </article> 