---
title: "Mecha Sword"
order: 3
in_menu: false
---
![image du robot Mecha Sword](/images/32235247_1916623448369530_5118655144774336512_n.jpg)

La construction complète du robot Mecha Sword est sous licence open source.

Il est composée des éléments suivantes : 

#### mécanique

* Châssis en bois (découpe laser)

* Jantes en bois (découpe laser)

* Supports moteurs en bois (découpe laser)

* Support de pile en bois (découpe laser)

#### Électronique

* Deux moteurs 90 RPM

* Carte électronique Dagu MKII

* Deux capteurs Grove suivie de ligne

#### Programmation [Bientôt]

#### PLANS 2D [Bientôt] 